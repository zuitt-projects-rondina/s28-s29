//First we load the expressjs module into out application and ssaved it in a variable called express

const express = require('express')

//create an app with expressjs
//This cretaes an application that uses express and store it as app
// app is our server

const app = express();

const port = 4000;

//middleware
//express.json() is a method from express which allow us to handle the streaming of data and automatically parse the incoming JSON from our req body
// app.use is used to run a method or another function for our expressjs api
app.use(express.json());

// Mock data
let users = [
		{
			username: "BMadrigal",
			email: "fateReader@gmail.com",
			password: "weDontTalkAboutMe"
		},
		{
			username: "LuisaMadrigal",
			email: "strongSis@gmail.com",
			password: "pressure"
		}
];

let items = [
		{
			name : "roses",
			price: 170,
			isActive: true	
		},
		{
			name: "tulips",
			price: 250,
			isActive: true
		}
];

// Express has a methods to use as routes corresponding to HTTP methods
// app.get(<endpoint>)

app.get('/', (req, res) => {

		// Once the route is accessed, we can send a response with the use of res.send()
		// res.send() actually combines writeHead() and end()
			// used to send a respnse to the client and ends the request
		res.send('Hello from Express JS API!')
});

app.get('/greeting',(req, res) =>{

		res.send(`Hello from Batch169-rondina`)
});

app.get('/users', (req, res) => {
	// res.send() stringifies it for you
	res.send(users)
});

// How do we get data from client as a requestbody?

app.post('/users', (req, res) => {

		console.log(req.body);


		let newUser = {

			username : req.body.username,
			email : req.body.email,
			password  : req.body.password
		}

		users.push(newUser);
		console.log(users)

		res.send(users);

})

app.delete('/users', (req, res) => {

		users.pop();
		console.log(users);

		res.send(users)
});

// updating users route

app.put('/users/:index', (req, res) => {
		// req.body - this will contains updated pssword
		console.log(req.body)

		// req.params object which contains the  value in the url params
		// url params is captured by route parameter (:parameterName) and saved as propert in the req.params
		console.log(req.params);

		let index = parseInt(req.params.index);


		// get the user that we want to update with our index number from url params
		users[index].password = req.body.password


		// send the updated user to the client
		// provide the index variable to be the index for the particular item in the array
		res.send(users[index]);
});


app.get('/users/getSingleUser/:index', (req, res) =>{

	let index = parseInt(req.params.index);

	res.send(users[index]);

})
/*MINI ACTIVITY*/

app.get('/items', (req, res) =>{

	res.send(items)
});

app.post('/items', (req, res) =>{

		let newItem = {
			 name : req.body.name,
			 price : req.body.price,
			 isActive : req.body.isActive
		};

		items.push(newItem);
		res.send(items);
})

app.put('/items/:index', (req, res) => {

		let index = parseInt(req.params.index)

		items[index].price = req.body.price

		res.send(items[index]);


});

// ==================== A C T I V I T Y ========================

app.get('/items/getSingleItem/:index', (req, res) =>{

	let index = parseInt(req.params.index);

	res.send(items[index]);

});

app.put('/items/archive/:index', (req, res) =>{

		let index = parseInt(req.params.index)

		items[index].isActive = req.body.isActive

		res.send(items[index]);

});

app.put('/items/activate/:index', (req, res) =>{

		let index = parseInt(req.params.index)

		items[index].isActive = req.body.isActive

		res.send(items[index]);

});





//listen method, server listen to the assigned port
app.listen(port, () => console.log(`Server is running at port ${port}`))